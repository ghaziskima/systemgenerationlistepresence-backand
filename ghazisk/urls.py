"""ghazisk URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from backAdmin import views
from django.conf.urls.static import static
from django.conf import settings
router = routers.DefaultRouter()
router.register(r'sections', views.SectionViewSet)
router.register(r'groupes', views.GroupeViewSet)
router.register(r'Materials', views.MaterialViewSet)
router.register(r'professors', views.ProfessorViewSet)
router.register(r'students', views.StudentViewSet)
router.register(r'classrooms', views.ClassroomViewSet)
router.register(r'meetings', views.MeetingViewSet)
router.register(r'timetables', views.TimetableViewSet)
router.register(r'states', views.StateViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('startdetecter/', views.start_detecter),

    path('signprof/', views.sign_prof),
]
urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)