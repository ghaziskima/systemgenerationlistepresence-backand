from django.db import models
from django.forms.models import model_to_dict
from datetime import datetime

#Section
class Section(models.Model):
     section_name = models.TextField(blank=True,null=True,default=None)
     level = models.IntegerField()
     specialty = models.TextField(blank=True,null=True,default=None)
#Groupe
class Groupe(models.Model):
     section = models.ForeignKey(Section,related_name='section_id_1', on_delete=models.CASCADE,blank=True,default=None,null=True)
     groupe_name = models.TextField(blank=True,null=True,default=None)
#Matière
class Material(models.Model):
    section = models.ForeignKey(Section,related_name='section_id_2', on_delete=models.CASCADE,blank=True,default=None,null=True)
    material_name =models.TextField(blank=True,null=True,default=None)
    types = models.TextField(blank=True,null=True,default=None)
#Professeur
class Professor(models.Model):
     first_name = models.TextField(blank=True,null=True,default=None)
     second_name = models.TextField(blank=True,null=True,default=None)
     nic = models.TextField(blank=True,null=True,default=None)
     birthday= models.DateField(auto_now=False, auto_now_add=False)
     email = models.TextField(blank=True,null=True,default=None)
     address = models.TextField(blank=True,null=True,default=None)
#Etudiant
class Student(models.Model):

     def user_directory_path(instance, filename):
        
         qs = Groupe.objects.get(id=instance.groupe.id)

         sc = Section.objects.get(id=model_to_dict(qs)['section'])
         
         print(model_to_dict(instance))
         return '{0}/{1}'.format('2020/'+model_to_dict(sc)['section_name']+'/'+ instance.groupe.groupe_name, str(instance.nic)+'--'+str(instance.first_name)+'--'+str(instance.second_name)+'--'+filename)
     groupe = models.ForeignKey(Groupe,related_name='groupe_id', on_delete=models.CASCADE,blank=True,default=None,null=True)
     first_name = models.TextField(blank=True,null=True,default=None)
     second_name = models.TextField(blank=True,null=True,default=None)
     nic = models.TextField(unique=True)
     birthday= models.DateField(auto_now=False, auto_now_add=False)
     email = models.TextField(blank=True,null=True,default=None)
     address = models.TextField(blank=True,null=True,default=None)
     image = models.ImageField(blank=True,null=True,default=None,upload_to=user_directory_path)




#Salle
class Classroom(models.Model):
    classroom_name = models.TextField(blank=True,null=True,default=None)
    iP_cam = models.TextField(blank=True,null=True,default=None)
#Seance
class Meeting(models.Model):

  
    material = models.ForeignKey(Material,related_name='material_id', on_delete=models.CASCADE,blank=True,default=None,null=True)
    classroom = models.ForeignKey(Classroom,related_name='classroom_id', on_delete=models.CASCADE,blank=True,default=None,null=True)
    professor = models.ForeignKey(Professor,on_delete = models.CASCADE,related_name="professor_id",blank=True,null=True,default=None)      
    weekdays = models.TextField(blank=True,null=True,default=None)
    meeting_name = models.TextField(blank=True,null=True,default=None)
    diet =models.TextField(blank=True,null=True,default=None)
    types =models.TextField(blank=True,null=True,default=None)
    start= models.TimeField(blank=True,null=True,default=None)
    end= models.TimeField(blank=True,null=True,default=None)
#Emploi
class Timetable(models.Model):
    
    groupe = models.ForeignKey(Groupe,related_name='groupe_id_1',on_delete = models.CASCADE,blank=True,default=None,null=True) 
    meeting=models.ManyToManyField(Meeting,related_name="meeting_id_1",blank=True,default=None,null=True)
#Etat
class State(models.Model):
    presence = models.TextField(blank=True,null=True,default=None)
    emotion = models.TextField(blank=True,null=True,default=None)
    student = models.ForeignKey(Student,on_delete = models.CASCADE,related_name="student_id",blank=True,default=None,null=True) 
    meeting = models.ForeignKey(Meeting,related_name='meeting_id_2', on_delete=models.CASCADE,blank=True,default=None,null=True)
    daydate=models.DateTimeField(blank=True,null=True,default=None)
   