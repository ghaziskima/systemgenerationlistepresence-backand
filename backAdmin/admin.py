from django.contrib import admin
from backAdmin.models import * 
# Register your models here.


admin.site.register(Section)
admin.site.register(Groupe)
admin.site.register(Material)
admin.site.register(Professor)
admin.site.register(Student)
admin.site.register(Classroom)
admin.site.register(Meeting)
admin.site.register(Timetable)
admin.site.register(State)

