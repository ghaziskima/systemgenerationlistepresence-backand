from rest_framework import serializers
from backAdmin.models import *
from django.forms.models import model_to_dict


#Section
class SectionSerializer(serializers.ModelSerializer):
     #section_id_1= GroupeSerializer(many=True, read_only=True)
    
     class Meta:
         model = Section
         fields='__all__'


#Matiere
class MaterialSerializer(serializers.ModelSerializer):
     class Meta:
         model = Material
         fields='__all__'  
#Professeur
class ProfessorSerializer(serializers.ModelSerializer):
     class Meta:
         model = Professor
         fields='__all__'   
#Etudiant
class StudentSerializer(serializers.ModelSerializer):
     class Meta:
         model = Student
         fields='__all__'  
#Groupe         
class GroupeSerializer(serializers.ModelSerializer):


    sections = serializers.SerializerMethodField('data_section')
    #groupe_id= StudentSerializer(many=True, read_only=True)

  

    def data_section(self, groupe):

        if(groupe.section):

            qs = Section.objects.filter(id=model_to_dict(groupe.section)['id'])
            serializer = SectionSerializer(instance=qs, many=True)
            return serializer.data
        return

    class Meta:
         model = Groupe
         fields='__all__'            
#Salle
class ClassroomSerializer(serializers.ModelSerializer):
     class Meta:
         model = Classroom
         fields='__all__'  
#Seance
class MeetingSerializer(serializers.ModelSerializer):


    material2 = serializers.SerializerMethodField('data_material')
    classroom2 = serializers.SerializerMethodField('data_classroom')
    professor2 = serializers.SerializerMethodField('data_professor')
  
    def data_material(self, x):

        if(x.material):

            qs = Material.objects.filter(id=model_to_dict(x.material)['id'])
            serializer = MaterialSerializer(instance=qs, many=True)
            return serializer.data[0]
        return

    def data_classroom(self, x):

        if(x.classroom):

            qs = Classroom.objects.filter(id=model_to_dict(x.classroom)['id'])
            serializer = ClassroomSerializer(instance=qs, many=True)
            return serializer.data[0]
        return

    def data_professor(self, x):

        if(x.professor):

            qs = Professor.objects.filter(id=model_to_dict(x.professor)['id'])
            serializer = ProfessorSerializer(instance=qs, many=True)
            return serializer.data[0]
        return        



    class Meta:
         model = Meeting
         fields='__all__'  
#Emploi
class TimetableSerializer(serializers.ModelSerializer):

     
     
     meeting2 = serializers.SerializerMethodField('data_meeting')

    
     
     def data_meeting(self, x):

        if(not x.meeting is None):
            tab=[]
           
            for i in x.meeting.all():
            

              qs = Meeting.objects.filter(id=i.id)
              serializer = MeetingSerializer(instance=qs, many=True)

              tab.append(serializer.data[0])
            return tab
        return 
     class Meta:
         model = Timetable
         fields='__all__'  
#Etat
class StateSerializer(serializers.ModelSerializer):
     student2 = serializers.SerializerMethodField('data_student')
     def data_student(self, x):

        if(x.student):

            qs = Student.objects.filter(id=model_to_dict(x.student)['id'])
            serializer = StudentSerializer(instance=qs, many=True)
            return serializer.data[0]
        return
     class Meta:
         model = State
         fields='__all__' 
        
         