import cv2
from matplotlib.pyplot import imshow
import matplotlib.pylab as plt
import face_recognition
import glob
from backAdmin.models import *
from django.forms.models import model_to_dict

def startmeeting(sc_name,groupe_name,id_meeting,date_meeting):
      

     
        cap = cv2.VideoCapture(0)
        results = []


        if not (cap.isOpened()):

            print("Could not open video device")



        images = []
        encodings=[]
        
        for file in glob.glob('./media/2020/'+str(sc_name)+'/'+str(groupe_name)+'/*.jpg'):
            
            student_data= (file.replace("./media/2020/"+str(sc_name)+"/"+str(groupe_name)+"\\",""))[:-4].split('--')
            print(student_data)
            student=Student.objects.filter(nic=student_data[0])
            for t in student:
               student_id=model_to_dict(t)['id']
               

            
            state_student= State.objects.create(student_id=student_id,meeting_id=id_meeting,daydate=date_meeting,emotion='sad',presence='no') 


            images.append("".join(file.split('.')[0:-1]))
            encodings.append(face_recognition.face_encodings(face_recognition.load_image_file(file))[0])


        count_faces = 0

        while(True):

        

            ret, frame = cap.read()
            
            face_locations = face_recognition.face_locations(frame)
            name = "No face detected !"
            if(len(face_locations) != count_faces):
                
                face_encodings = face_recognition.face_encodings(frame, face_locations)
                for face_encoding in face_encodings:
                    matches = face_recognition.compare_faces(encodings, face_encoding)

                    name = "Unknown Person"
                    if True in matches:
                        first_match_index = matches.index(True)
                        name = images[first_match_index]

                        print(name)

                        student_data= (file.replace("./media/2020/"+str(sc_name)+"/"+str(groupe_name)+"\\",""))[:-4].split('--')
                        print(student_data)

                        student=Student.objects.filter(nic=student_data[0])
                        for t in student:
                           student_id=model_to_dict(t)['id']
                        state_student= State.objects.filter(student=student_id).filter(daydate=date_meeting).update(student_id=student_id,meeting_id=id_meeting,daydate=date_meeting,emotion='happy',presence='yes') 
                        
                        
                    
                        if name not in results:
                            results.append(name)

                            
                            
            
                
                count_faces = len(face_locations)
                print(f'There are {len(face_locations)} people in this image {count_faces}')
            


            

            cv2.imshow('preview',frame)
        

            if cv2.waitKey(1) & 0xFF == ord('q'):

                break

        
        cap.release()

        cv2.destroyAllWindows()
        print(results)