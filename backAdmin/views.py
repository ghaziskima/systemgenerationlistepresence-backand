from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from backAdmin.serializers import *
from backAdmin.models import *
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework import status
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
import json
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
import os

from backAdmin.open_cam import startmeeting
from threading import Thread
from django.http import JsonResponse

# Section


class SectionViewSet(viewsets.ModelViewSet):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        

        dirName = serializer.data.get('section_name')


        try:
          # Create target Directory
          os.makedirs("./media/2020/"+dirName)
          print("Directory ", dirName,  " Created ")
        except FileExistsError:
             print("Directory ", dirName,  " already exists")
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

# Groupe


class GroupeViewSet(viewsets.ModelViewSet):
    queryset = Groupe.objects.all()
    serializer_class = GroupeSerializer
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        

        dirName = serializer.data.get('groupe_name')
        parentName=serializer.data.get('sections')[0]['section_name']


        try:
          # Create target Directory
          os.makedirs("./media/2020/"+parentName+"/"+dirName)
          print("Directory ", dirName,  " Created ")
        except FileExistsError:
             print("Directory ", dirName,  " already exists")
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
# Matiere


class MaterialViewSet(viewsets.ModelViewSet):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer

# Professeur


class ProfessorViewSet(viewsets.ModelViewSet):
    queryset = Professor.objects.all()
    serializer_class = ProfessorSerializer
# Etudiant


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['email']
    filterset_fields = ['groupe']






# Salle


class ClassroomViewSet(viewsets.ModelViewSet):
    queryset = Classroom.objects.all()
    serializer_class = ClassroomSerializer

# Seance


class MeetingViewSet(viewsets.ModelViewSet):
    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['professor']
    filterset_fields = ['professor']
# Emploi


class TimetableViewSet(viewsets.ModelViewSet):
    queryset = Timetable.objects.all()
    serializer_class = TimetableSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['groupe']
    filterset_fields = ['groupe']

    def create(self, request, *args, **kwargs):

        id_meeting = []
        qs = Timetable.objects.filter(groupe=request.data.get("groupe")['id'])
        last_timetable = TimetableSerializer(instance=qs, many=True)
        headers = self.get_success_headers(last_timetable.data)

        if(len(last_timetable.data) > 0):
            for v in range(42):

                old_meeting_id = last_timetable.data[0]['meeting'][v]
                old_meeting = model_to_dict(
                    Meeting.objects.get(id=old_meeting_id))

                new_meeting = request.data.get('meeting')[v]

                if(new_meeting['material']):
                    if('id' in new_meeting['material']):

                        new_meeting['material'] = new_meeting['material']['id']

                if(not new_meeting['classroom']is None):
                    if('id' in new_meeting['classroom']):
                        new_meeting['classroom'] = new_meeting['classroom']['id']

                if(not new_meeting['professor'] is None):
                    if('id' in new_meeting['professor']):
                        new_meeting['professor'] = new_meeting['professor']['id']

                new_meeting['id'] = old_meeting_id

                serializer = MeetingSerializer(
                    Meeting.objects.get(id=old_meeting_id), data=new_meeting)
                if serializer.is_valid(raise_exception=True):
                    meeting = serializer.save()

            return Response(last_timetable.data, status=status.HTTP_201_CREATED, headers=headers)

        else:

            for i in request.data.get('meeting'):

                if(i['material']):

                    if('id' in i['material']):

                        i['material'] = i['material']['id']

                if('classroom' in i):
                    if('id' in i['classroom']):
                        i['classroom'] = i['classroom']['id']

                if('professor' in i):
                    if('id' in i['professor']):
                        i['professor'] = i['professor']['id']

                serializer = MeetingSerializer(data=i)
                if serializer.is_valid():
                    meeting = serializer.save()
                    id_meeting.append(meeting.id)

                else:
                    print("noooo")

            data_timetible = {"groupe": request.data.get(
                "groupe")['id'], "meeting": id_meeting}

            serializer = self.get_serializer(data=data_timetible)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response("ok")


# Etat
class StateViewSet(viewsets.ModelViewSet):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    search_fields = ['daydate']
    filterset_fields = ['daydate']


@csrf_exempt
@api_view(["POST"])
def sign_prof(request):

  try:  
    prof=Professor.objects.filter(email=str(request.data.get('email'))).filter(nic=str(request.data.get('nic')))

    for t in prof:
        prof_data=model_to_dict(t)
        
    prof_id=prof_data['id']
    prof_meeting=Meeting.objects.filter(professor=prof_id)
    meeting=[]
    for v in prof_meeting:
      m=model_to_dict(v)  
      if(m['material']):
          m['material']=model_to_dict(Material.objects.get(id=m['material']))
      if(m['classroom']):
          m['classroom']=model_to_dict(Classroom.objects.get(id=m['classroom']))
      if(m['professor']):
          m['professor']=model_to_dict(Professor.objects.get(id=m['professor']))     

      
      meeting.append(m)

  except:

      return JsonResponse({'prof':'pas de prof'},status=404)




  return JsonResponse({'prof':prof_data, 'meeting': meeting},status=200)



@csrf_exempt
@api_view(["POST"])
def start_detecter(request):

    
    id_meeting=request.data.get('id')
   
    timetable=Timetable.objects.filter(meeting=id_meeting)

    for t in timetable:
      groupe_id=model_to_dict(t)['groupe']


    qs = Groupe.objects.get(id=groupe_id)

    sc = Section.objects.get(id=model_to_dict(qs)['section']) 
    sc_name=model_to_dict(sc)['section_name']
    gr_name=model_to_dict(qs)['groupe_name']
    
    now = datetime.now()
   

    thread = Thread(target = startmeeting, args = (sc_name,gr_name,id_meeting,now ))
    thread.start()
    


    
    

    return Response(now)
